import json
import datetime
import requests
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy import Table, Column, Integer, String, MetaData, text
import time
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()



datacollected={}

class ActualTable(Base):
   __tablename__ = 'historicdata_TCS'
   
   historyid = Column(Integer, primary_key=True)
   exchangename = Column(String)
   symbol = Column(String)
   timestamp = Column(String)
   open = Column(String)
   high = Column(String)
   low = Column(String)
   close = Column(String)



class datasaver:
    def __init__(self,symbols):
        engine = create_engine('sqlite:///myalgotest.db', echo = True)
        Session = sessionmaker(bind = engine)
        self.session = Session()
        self.engine=engine
        
        self.createobjects("TCS")
        
    def createobjects(self,symbolname):
        conn = self.engine.connect()
        # tablename = 'historicdata_'+symbolname
        # t = text("drop table if exists "+tablename)
        # result = self.conn.execute(t)
        
        t = text("delete from historicdata_TCS")
        result = conn.execute(t)

        
        

        # self.tables[symbolname] = Table(
        #     tablename, self.meta, 
        #     Column('historyid', Integer, primary_key = True), 
        #     Column('exchangename', String), 
        #     Column('symbol', String), 
        #     Column('timestamp', String), 
        #     Column('open', String), 
        #     Column('high', String), 
        #     Column('low', String), 
        #     Column('close', String)
        #     )
        # self.meta.create_all(self.engine)

        # print("************")
        # print(self.tables)
        # print("-=============")

# selectedexchangetype,symbol,intervalType

    def searchall(self,datadic):
        res = self.session.query(ActualTable).all()
        # res = table.select()
        return res

    def savetodb(self,datadic):
        # table = self.session.query(ActualTable)
        c1 = ActualTable(close="z",exchangename="no",high="1",low="1",open="1",symbol="1",timestamp="1")
        self.session.add(c1)
        self.session.commit()



datacollected={
            "exchangename":"NSE",
            "symbol":"TCS",
            "datarecieved":{
                "code": 200,
                "status": "OK",
                "timestamp": "2018-11-13T12:44:38+05:30",
                "message": "historical-data",
                "data": [
                    "1541735100000,287,287,283,284.3,5646582",
                    "1541738700000,284.3,287.35,284.2,285.85,4684886",
                    "1541742300000,285.85,286.55,285.3,286.05,1663518",
                    "1541745900000,286.05,286.05,283.55,284.1,1676312",
                    "1541749500000,284,284.5,282.5,283.65,1897609",
                    "1541753100000,283.55,284.25,282.85,283.6,2281085",
                    "1541756700000,283.4,283.75,282.85,283.1,1340651"
                ]
            }
        }    

ds = datasaver(["TCS"])
starttime = time.time()
startdate = datetime.datetime.now();



for _ in range(10000):
    ds.savetodb(datacollected)
# res = ds.searchall(datacollected)
# print("type=======")
# print(len(res))
# print("type******")
# for row in res:
#     print(row.close)




for _ in range(10000):
    ds.savetodb(datacollected)
    
    ds.session.query(ActualTable).delete()
    ds.session.commit()

# ORM - 3000 times inserting and deleting
# results: 39.12 seconds, 31.49 seconds,34.18 seconds
# CORE - 3000 times inserting and deleting
# results: 36.82 seconds by core, 31.62 seconds,32.19
# ORM - 3000 times inserting, again inserting and deleting
# results: 52.75
# Core - 3000 times inserting, again inserting and deleting
# results: 50.64
# ORM - 10000 times inserting, again inserting and deleting
# results: 181.58, 179,173, 181, 166
# Core - 10000 times inserting, again inserting and deleting
# results: 172, 305 secon, 197, 168

endtime = time.time()
enddate = datetime.datetime.now()

print("starttime is ",startdate)
print("endtime is ",enddate)
print("total time is ",endtime-starttime)