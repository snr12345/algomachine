from sqlalchemy import create_engine,Column,Table,Integer,String
engine = create_engine('sqlite:///learning.db', echo = True)

from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind = engine)

class MyTableClass(Base):
	__tablename__ = 'tablename'
	myFirstCol = Column(Integer, primary_key=True)
	mySecondCol = Column(Integer)

class Customers(Base):
   __tablename__ = 'testtable'
   
   id = Column(Integer, primary_key = True)
   name = Column(String)
   address = Column(String)
   email = Column(String)

# Base.metadata.create_all(engine)

session = Session()

c1 = Customers(name = 'Ravi Kumar', address = 'Station Road Nanded', email = 'ravi@gmail.com')

session.add(c1)
session.commit()


def NewTablebyFunction():
    attr_dict = {'__tablename__': 'newtablez',
	     'myFirstCol': Column(Integer, primary_key=True),
	     'mySecondCol': Column(Integer)}
    Base = declarative_base()

    newtablez = type('MyTableClass', (Base,), attr_dict) 

    Base.metadata.create_all(engine)

def gettheschema(tablenam):
    attr_dict = {'__tablename__': tablename,
	     'myFirstCol': Column(Integer, primary_key=True),
	     'mySecondCol': Column(Integer)}
    Base = declarative_base()

    newtablez = type('MyTableClass', (Base,), attr_dict) 
    
    session.add(
        newtablez(mySecondCol="satya")
    )

    session.commit()

NewRow()