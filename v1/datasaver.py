import json
import datetime
import requests
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy import Table, Column, Integer, String, MetaData, text


datacollected={}



class datasaver:
    def __init__(self,symbols):
        self.engine = create_engine('sqlite:///myalgotest.db', echo = True)
        self.meta = MetaData()
        self.tables={}
        for symbol in symbols:
            self.createobjects(symbol)
        
    def createobjects(self,symbolname):
        self.conn = self.engine.connect()
        tablename = 'historicdata_'+symbolname
        # t = text("drop table if exists "+tablename)
        # result = self.conn.execute(t)

        self.tables[symbolname] = Table(
            tablename, self.meta, 
            Column('historyid', Integer, primary_key = True), 
            Column('exchangename', String), 
            Column('symbol', String), 
            Column('timestamp', String), 
            Column('open', String), 
            Column('high', String), 
            Column('low', String), 
            Column('close', String)
            )
        self.meta.create_all(self.engine)

        print("************")
        print(self.tables)
        print("-=============")

# selectedexchangetype,symbol,intervalType

    def savetodb(self,datadic):
        table = self.tables[datadic["symbol"]]
        ins = table.insert().values(close="z",exchangename="no",high="1",low="1",open="1",symbol="1",timestamp="1")
        # print(ins)
        self.conn.execute(ins)

        
    