from importlib import reload


import backfiller
reload(backfiller)

import datasaver
reload(datasaver)

import datacollector
reload(datacollector)

import lookups
reload(lookups)

import datetime
import time


# variables
selectedintervalType=None
selectedcandleperiod=None
starttime=None
endtime=None

# user settings
selectedfilltype="backfill"
selectedexchangetype="NSE"
selectedintervalType = "5min"
selectedrangenumber = 200
selectedstarttime = '25-09-2019'
selectedendtime = '26-09-2019'
selectedSymbols=["HDFCBANK","TCS","ITC"]

# database settings
intervalType = lookups.intervalTypes[selectedintervalType]

if selectedfilltype=="backfill":
    # getting data based on number of candles
    # backfill options
    intervalTypemilliseconds=lookups.intervalTypeMS[selectedintervalType]
    backfillmillisecnods = selectedrangenumber*intervalTypemilliseconds
    timenow=datetime.datetime.now()
    
    starttime = timenow-datetime.timedelta(milliseconds=backfillmillisecnods)
    endtime = timenow

    starttime = datetime.datetime(starttime.year,starttime.month,starttime.day)
    endtime = datetime.datetime(endtime.year,endtime.month,endtime.day)
    
else:
    #historicdatainsert
    starttime = datetime.datetime.strptime(selectedstarttime, '%d-%m-%Y').date()
    endtime = datetime.datetime.strptime(selectedendtime, '%d-%m-%Y').date()


print(starttime)
print(endtime)

dc = datacollector.datacollector()
ds = datasaver.datasaver(selectedSymbols)

def datapulled(data):
    ds.savetodb(data)


for symbol in selectedSymbols:
    data = dc.getdataBySymbol(selectedexchangetype,symbol,intervalType,starttime,endtime,datapulled)

# pul = datautil.puller()
# subprocess.call()
# pul.getLoginURLFromUpstox()
# pul.setUpstoxToken("fc71723cb4e9a0bee23efb8efdb7fbca2c3b9a1c")
# pul.getProfile()
# data = pul.getHistoricalData(selectedSymbol,intervalType,starttime,endtime)