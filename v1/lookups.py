intervalTypeMS = {
  "1min": 60000,
  "5min": 300000,
  "15min": 900000,
  "30min": 1800000,
  "1hour": 3600000,
}

intervalTypes = {
  "1min": 1,
  "3min": 3,
  "5min": 5,
  "10min": 10,
  "15min": 15,
  "30min": 30,
  "1hour": 60,
  "day": "day",
  "week": "week",
  "month": "month",

}

