import json
import datetime
import requests
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy import Table, Column, Integer, String, MetaData, text
import time


datacollected={}



class datasaver:
    def __init__(self,symbols):
        self.engine = create_engine('sqlite:///myalgotest.db', echo = True)
        self.meta = MetaData()
        self.tables={}
        self.createobjects("TCS")
        
    def createobjects(self,symbolname):
        self.conn = self.engine.connect()
        # t = text("drop table if exists "+tablename)
        # result = self.conn.execute(t)
        # t = text("delete from historicdata_TCS")
        # result = self.conn.execute(t)

        self.tables["TCS"] = Table(
            "historicdata_TCS", self.meta, 
            Column('historyid', Integer, primary_key = True), 
            Column('exchangename', String), 
            Column('symbol', String), 
            Column('timestamp', String), 
            Column('open', String), 
            Column('high', String), 
            Column('low', String), 
            Column('close', String)
            )
        self.meta.create_all(self.engine)

        # print("************")
        # print(self.tables)
        # print("-=============")

# selectedexchangetype,symbol,intervalType

    def searchall(self,datadic):
        table = self.tables["TCS"]
        qry = table.select()
        res = self.conn.execute(qry)
        return res

    def savetodb(self,datadic):
        table = self.tables["TCS"]
        ins = table.insert().values(close="z",exchangename="no",high="1",low="1",open="1",symbol="1",timestamp="1")
        # print(ins)
        self.conn.execute(ins)


datacollected={
            "exchangename":"NSE",
            "symbol":"TCS",
            "datarecieved":{
                "code": 200,
                "status": "OK",
                "timestamp": "2018-11-13T12:44:38+05:30",
                "message": "historical-data",
                "data": [
                    "1541735100000,287,287,283,284.3,5646582",
                    "1541738700000,284.3,287.35,284.2,285.85,4684886",
                    "1541742300000,285.85,286.55,285.3,286.05,1663518",
                    "1541745900000,286.05,286.05,283.55,284.1,1676312",
                    "1541749500000,284,284.5,282.5,283.65,1897609",
                    "1541753100000,283.55,284.25,282.85,283.6,2281085",
                    "1541756700000,283.4,283.75,282.85,283.1,1340651"
                ]
            }
        }    

ds = datasaver(["TCS"])
starttime = time.time()
startdate = datetime.datetime.now();



# for _ in range(10000):
#     ds.savetodb(datacollected)
res = ds.searchall(datacollected)
# print(type(res))


for row in res:
    print(row.close)

# for _ in range(10000):
#     ds.savetodb(datacollected)
    
#     table = ds.tables["TCS"]
#     stmt = table.delete()
#     ds.conn.execute(stmt)

endtime = time.time()
enddate = datetime.datetime.now()

print("starttime is ",startdate)
print("endtime is ",enddate)
print("total time is ",endtime-starttime)