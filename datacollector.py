import json
import datetime
import requests
from upstox_api.api import *

s=None
access_token=None
# url = "https://api.upstox.com/historical/nse_eq/HDFCBANK/1"

# querystring = {"end_date":"26-09-2019","start_date":"25-09-2019"}

# headers = {
#     'x-api-key': "oeACc9ejKG1C1A7Ju6Fj97fabaV11ZgQ8sLA24Bk",
#     'Authorization': "Bearer 7251bd2dfbd627307187f501e4664cfa76935d08",
#     'User-Agent': "PostmanRuntime/7.17.1",
#     'Accept': "*/*",
#     'Cache-Control': "no-cache",
#     'Postman-Token': "d6fbb645-4510-46f9-b92e-0959860b1a10,98b09f9c-898e-429a-a7ec-1a507f421c5b",
#     'Host': "api.upstox.com",
#     'Accept-Encoding': "gzip, deflate",
#     'Cookie': "sid=s%3A6NEm6wMMVEmQwZM5-AAXCykQ8bFvql_c.PNNeI1ksssY%2BIFQuFfCPJ47sCojFsvD99mSvLlduyN4",
#     'Connection': "keep-alive",
#     'cache-control': "no-cache"
#     }

# response = requests.request("GET", url, headers=headers, params=querystring)

# print(response.text)


def getdatanseeq(Symbol):
    print(Symbol)


def getdata(Symbol,intervalType,StartDate,EndDate):
    
    url = "https://api.upstox.com/historical/nse_eq/"+Symbol+"/1"
    querystring = {"end_date":EndDate,"start_date":StartDate}
    headers = {
    'x-api-key': "oeACc9ejKG1C1A7Ju6Fj97fabaV11ZgQ8sLA24Bk",
    'Authorization': "Bearer 7251bd2dfbd627307187f501e4664cfa76935d08"
    }
    response = requests.request("GET", url, headers=headers, params=querystring)

    # dataset = '{"a":1,"b":2}'
    dr = json.loads(response.text)
    return dr

def getLoginURLFromUpstox():
    global s
    s = Session ('oeACc9ejKG1C1A7Ju6Fj97fabaV11ZgQ8sLA24Bk')
    s.set_redirect_uri ('http://127.0.0.1')
    s.set_api_secret ('g9sm7kctwk')
    return s.get_login_url()

def setCode(code):
    global s
    s.set_code (code)

def setUpstoxToken():
    global s
    global access_token
    access_token = s.retrieve_access_token()
    print('token got',access_token)
    
    

def getDataUsingToken():
    global access_token
    u = Upstox ('oeACc9ejKG1C1A7Ju6Fj97fabaV11ZgQ8sLA24Bk', access_token)
    print (u.get_profile())
