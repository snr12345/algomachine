using System;
using System.Collections;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;   


namespace v4
{
    class UpstoxTest{

        string apiKey="oeACc9ejKG1C1A7Ju6Fj97fabaV11ZgQ8sLA24Bk";
        string apiSecret = "g9sm7kctwk";
        string redirect_uri="http://127.0.0.1";

        string auth_code="c08ddb9eb57939351c21e7f921fa304baae090fa";

        string valid_token="af4492c7480d4c6ea608a5b66227a4d5f2f2275f";

        public void urltest(){
            UpStoxUtil ut = new UpStoxUtil(apiKey,apiSecret,redirect_uri);
                Console.WriteLine(ut.getloginurl());
        }
        public void tokentest(){
            UpStoxUtil ut = new UpStoxUtil(apiKey,apiSecret,redirect_uri);
            string token = ut.getToken(auth_code);
            Console.WriteLine(token);
        }

        public void datatest(){
            Console.WriteLine("tesining the code part");
            UpStoxUtil ut = new UpStoxUtil(apiKey,apiSecret,redirect_uri);
            ut.setToken(valid_token);
            string datarecived = ut.getStockData("historical","nse_eq","TCS","1","09-09-2019","10-09-2019");
            StockResponse data =JsonConvert.DeserializeObject<StockResponse> (datarecived);
            // var totalresponse = JObject.Parse(datarecived);
            //     var datapart = totalresponse["data"].ToString();
            Console.WriteLine(data.code);
        }

        public void serialtest(){
            string[] x = new string[] { "Small", "Medium", "Large" };
            var data =JsonConvert.SerializeObject(x);
            Console.WriteLine(data);
            
        }

        public void nullcheck(){
            string myname=null;

            if(myname!=null && myname.ToString()=="satya"){
                Console.WriteLine("printing {0}",myname);
            }
        }

        public void minmaxcheck(){
            List<int> ls = new List<int>();
            ls.Add(1);
            ls.Add(2);
            Console.WriteLine(ls[0]);
        }

        public void inttest(){
            Console.WriteLine(Convert.ToInt64("1569919500000"));
        }

public  DateTime UnixTimeStampToDateTime( double unixTimeStamp )
{
    unixTimeStamp = unixTimeStamp/1000;
    // Unix timestamp is seconds past epoch
    System.DateTime dtDateTime = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
    dtDateTime = dtDateTime.AddSeconds( unixTimeStamp ).ToLocalTime();
    return dtDateTime;
}

public void updateListTest(){
    List<Signal> ls = new List<Signal>();
    ls.Add(new Signal(){symbolName="TCS"});
    ls.Add(new Signal(){symbolName="ITC"});
    ls.Add(new Signal(){symbolName="HDFCBANK"});

    var lastsignal = ls.FindLast(x=>x.symbolName=="ITC");
    lastsignal.symbolName="google";

    foreach(Signal s in ls){
        Console.WriteLine(s.symbolName);
    }
    
}
        public void datetotimestamp(){
            Console.WriteLine(UnixTimeStampToDateTime(1569919500000));
        }

        public void StrategyTester(){
            AnalysisData ad = new AnalysisData();
            ad.SymbolName="TCS";
            ad.ExchangeName="NSE";
            ad.IntervalType = "1";
            ad.dataforanalysis = new List<OHLC>();

            //Uptrend High breakout
            ad.dataforanalysis.Add(new OHLC(){open=9.5,high=10.5,low=9,close=10,timestammp=915});
            ad.dataforanalysis.Add(new OHLC(){open=10,high=11,low=9.5,close=10.5,timestammp=920});
            ad.dataforanalysis.Add(new OHLC(){open=10.5,high=10.99,low=9.5,close=10,timestammp=1010});
            ad.dataforanalysis.Add(new OHLC(){open=10.5,high=10.5,low=9.5,close=10,timestammp=1010});
            ad.dataforanalysis.Add(new OHLC(){open=10.5,high=10.99,low=9.5,close=10,timestammp=1010});
            ad.dataforanalysis.Add(new OHLC(){open=10,high=11.5,low=9.7,close=11,timestammp=1115});
            ad.dataforanalysis.Add(new OHLC(){open=11,high=12,low=10.5,close=11.5,timestammp=1210});
            // ad.dataforanalysis.Add(new OHLC(){open=12,high=12.5,low=11,close=11.5,timestammp=6});
            ad.dataforanalysis.Add(new OHLC(){open=11,high=11.5,low=10,close=10.5,timestammp=1113});
            ad.dataforanalysis.Add(new OHLC(){open=11,high=11.90,low=10,close=10.5,timestammp=1113});
            ad.dataforanalysis.Add(new OHLC(){open=11.5,high=13,low=10.5,close=12.5,timestammp=1220});
            ad.dataforanalysis.Add(new OHLC(){open=12.5,high=14,low=12,close=13.5,timestammp=1315});
            // expected output is 
            //  entry at 11
            // stop loss at 9.5
            // target at 11+(11-9.5) = 12.5
            
            StrategyAnalyzer sa = new StrategyAnalyzer();
            sa.Start(ad,"Highbreakout","full");
        }
        public void Execute(){
                // urltest();
                // tokentest();
                // datatest();
                // nullcheck();
                // minmaxcheck();
                // datetotimestamp();
                // serialtest();
                StrategyTester();
                // updateListTest();
        }
    }

}
