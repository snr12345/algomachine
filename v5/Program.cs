﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace v4
{
    class DBCreator
    {
        //test comments
        var x="testing";
    }

    class Processor
    {
        string selecteddatatype = default(string);
        string selectedexchangename = default(string);
        List<string> selectedintervalTypes = default(List<string>);
        string selectedstartdate = default(string);
        List<string> selectedSymbols = default(List<string>);
        DataCollector dataCollector ;

        public Processor(){
            dataCollector = new DataCollector();
        }
        public void StartBackFilling(CurrentProperties currentProperties)
        {
                DBConnector ds = new DBConnector(currentProperties);
                // ds.createobjects(tablename)
                
                foreach(DateRange daterange in currentProperties.DateRanges){
                    var totaldata = this.dataCollector.getStockData(currentProperties.LoadDataType,currentProperties.ExchangeName,currentProperties.SymbolName,currentProperties.IntervalType,daterange.StartDate,daterange.EndDate);
                    //incase of error, we are displaying through data collector
                    if(totaldata!=null){
                        ds.BulkSaveData(totaldata);
                    }
                    
                    

                    // Console.WriteLine("data recieved is {0}");
                }
                

        }

        public void Execute()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            List<Thread> totalthreads = new List<Thread>();
            
            // ds.CheckAndCreateDB("MyAlgoTest");

            this.selecteddatatype = "historical";
            this.selectedexchangename = "nse_eq";
            this.selectedintervalTypes = new List<string>(new string[] { "1","3","5","15","60" });
            this.selectedstartdate = "10-01-2019";
            this.selectedSymbols = new List<string>(new string[] { "TCS","HDFCBANK","ITC","DHFL","RELIANCE" });


            // Executer ex = new Executer();
            // ex.CreateDBs();
            
            DateTime startdate = DateTime.Parse(selectedstartdate);
            DateTime enddate = DateTime.Now.Date;

            



            
            
            Common cm = new Common();


            foreach (String symbol in selectedSymbols)
            {
                
                var dateranges = cm.SplitDays(new DateRange(){StartDate=startdate,EndDate=enddate});
                Console.WriteLine("printing date ranges");

                foreach (string intervalType in selectedintervalTypes)
                {
                    CurrentProperties c = new CurrentProperties();
                    c.SymbolName = symbol;
                    c.ExchangeName = selectedexchangename;
                    c.IntervalType = intervalType;
                    c.LoadDataType = selecteddatatype;
                    c.DateRanges=dateranges;
                    
                    c.DatabaseName="MyAlgoMachine";
            
                    Thread thread = new Thread(() => StartBackFilling(c));
                    thread.Start();
                    totalthreads.Add(thread);
                    // StartBackFilling(c);
                    
                }

                
            }

            foreach(Thread tr in  totalthreads)
            tr.Join();

            stopwatch.Stop();

            Console.WriteLine("{0} seconds with one transaction.",
            stopwatch.Elapsed.TotalSeconds);
        }
    }


    
    class Program
    {



        static void Main(string[] args)
        {


            
            // Processor p = new Processor();
            // p.Execute();
            
            UpstoxTest ut = new UpstoxTest();
            ut.Execute();

            // string x = "{0}";
            // Console.WriteLine(String.Format(x,"123"));

        }


    }
}
