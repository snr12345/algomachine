using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace v4
{
    public class AnalysisData{
        public string SymbolName;
        public string ExchangeName;
        public string IntervalType;
        public List<OHLC> dataforanalysis;

    }


    enum Strategy{
        highbreakout=0
    }
    public class OHLC{
        public int timestammp;
        public double open;
        public double high;
        public double low;
        public double close;

    }

    public class CurrentProperties{
        public string LoadDataType;
        public string SymbolName;
        public string ExchangeName;
        public string IntervalType;
        public string DatabaseName;
        public List<DateRange> DateRanges;
        // public List<Mycustom> mycustoms;

        public string TableName
        {
            get { return String.Format("{0}_{1}_{2}_{3}",SymbolName,LoadDataType,ExchangeName,IntervalType); }
        }
        
    }
    public class DateRange
    {
        public DateTime StartDate;
        public DateTime EndDate;
    }

    public class StockDataDetails{
        public string ExchangeName;
        public string SymbolName;
        public StockResponse datarecieved;
    }

    public class StockResponse{
        public string code;
        public string status;
        public string timestamp;
        public string message;

        public string[] data;
    }
    public class Common
    {

public  DateTime UnixTimeStampToDateTime( string strunixTimeStamp )
{
    double unixTimeStamp = Convert.ToDouble(strunixTimeStamp);
    unixTimeStamp = unixTimeStamp/1000;
    // Unix timestamp is seconds past epoch
    System.DateTime dtDateTime = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
    dtDateTime = dtDateTime.AddSeconds( unixTimeStamp ).ToLocalTime();
    return dtDateTime;
}
        public List<DateRange> SplitDays(DateRange dateRange)
        {
            var rangelist = new List<DateRange>();
            

            var startdate_Date = dateRange.StartDate;
            var enddate_Date = dateRange.EndDate;

            
            var difference = (enddate_Date - startdate_Date).TotalDays;

            
            var weeksneeded = Convert.ToInt32(difference) / 7;
            var daysleft = Convert.ToInt32(difference) % 7;

            if (weeksneeded == 0)
            {
                rangelist.Add(new DateRange() { StartDate = dateRange.StartDate, EndDate = dateRange.EndDate });
            }
            else
            {
                DateTime lastenddate = default(DateTime);
                DateTime currentstartdate = default(DateTime);
                DateTime currentenddate = default(DateTime);

                for (int i = 0; i < weeksneeded; i++)
                {
                    if (i == 0)
                    {
                        currentstartdate = startdate_Date;
                    }
                    else
                    {
                        currentstartdate = lastenddate.AddDays(1);
                    }

                    currentenddate = currentstartdate.AddDays(6);
                    lastenddate = currentenddate;

                    rangelist.Add(new DateRange() { StartDate = currentstartdate, EndDate = currentenddate });

                }


                currentstartdate = lastenddate.AddDays(1);
                currentenddate = currentstartdate.AddDays(daysleft);
                
                rangelist.Add(new DateRange() { StartDate = currentstartdate, EndDate = currentenddate });

            }



            return rangelist;
        }
    }

}
