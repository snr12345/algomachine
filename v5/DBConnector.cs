using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Data.Sqlite;


namespace v4
{
    public class DBConnector
    {

        private CurrentProperties dbproperties;
        public DBConnector(CurrentProperties dbproperties)
        {
            this.dbproperties = dbproperties;
            CreateTables();
        }
        private string _currentdbnameinuse = default(string);

        public string dbnameinuse
        {
            get { return _currentdbnameinuse; }
        }

        /// This is for future use, can be used to created multiple databases for each symbol
        // for now, only using single db, just trying to match python database
        // public void CreateDBs()
        // {
        //     List<string> ls = new List<string>();
        //     ls.Add("db1");
        //     ls.Add("db2");
        //     ls.Add("db3");
        //     foreach (var dbname in ls)
        //     {
        //         using (var client = new BloggingContext())
        //         {
        //             client.databasename = dbname;
        //             client.Database.EnsureCreated();
        //         }
        //     }

        // }

        public List<Int64> getMinMaxTimeStamps(string tablename)
        {
            List<Int64> minmax = new List<Int64>();

            var dbcon = String.Format("DataSource={0}.db", dbproperties.DatabaseName);
            using (var connection = new SqliteConnection(dbcon))
            {
                connection.Open();
                
                    string query = "select min(timestamp) as min,max(timestamp) as max from {0};";
                    query = String.Format(query, dbproperties.TableName);
                    // Console.WriteLine("new query is : {0}",newquery)

                    using (var command = connection.CreateCommand()){
                        // Create New Command Object using the connection.
                        // var command = connection.CreateCommand();
                        // Send the query to commandText property
                        command.CommandText = query;
                        using(SqliteDataReader rdr = command.ExecuteReader()){
                                while (rdr.Read()) 
                                {
                                    if(rdr["min"]==DBNull.Value)
                                    break;
                                    var min = Convert.ToInt64(rdr["min"]);
                                    var max = Convert.ToInt64(rdr["max"]);
                                    minmax.Add(min);
                                    minmax.Add(max);
                                }   
                        }
                    }
                    
                    //Open Connection

                    //Execute your command
                    // command.ExecuteReader();
                    //insert ito db

                
                connection.Close();
            }

            return minmax;
        }

        public void BulkSaveData_bkp(StockDataDetails sd)
        {
            string tablename = this.dbproperties.TableName;
            Console.WriteLine("saving to {0}", tablename);

            var stockdata = sd.datarecieved.data;

            var minmax = this.getMinMaxTimeStamps(tablename);

            var currentimte = DateTime.Now;

            if (minmax.Count > 0)
            {
                foreach (string stock in stockdata)
                {
                    var sepratedvalues = stock.Split(",");
                    var min = minmax[0];
                    var max = minmax[1];
                    

                    if (min > Convert.ToInt32(sepratedvalues[0]) || (Convert.ToInt32(sepratedvalues[0]) > max))
                    {
                        Console.WriteLine("inserted into db - from min max");
                        //insert ito db
                        // bulkdata.append({"timestamp":sepratedvalues[0],"open":sepratedvalues[1],"high":sepratedvalues[2],"low":sepratedvalues[3],"close":sepratedvalues[4],"dateadded":currenttime})
                    }

                }
            }
            else
            {
                // # duplicate code added just to improve the performance
                foreach (string stock in stockdata)
                {
                    var sepratedvalues = stock.Split(",");
                    Console.WriteLine("inserted into db - no min max");
                    //insert into db
                    // bulkdata.append({"timestamp":sepratedvalues[0],"open":sepratedvalues[1],"high":sepratedvalues[2],"low":sepratedvalues[3],"close":sepratedvalues[4],"dateadded":currenttime})
                }





            }
        }

        public void BulkSaveDataWithoutTrans(StockDataDetails sd)
        {
            string tablename = this.dbproperties.TableName;
            

            var stockdata = sd.datarecieved.data;

 

            if (stockdata.Length == 0)
                return;

            var minmax = this.getMinMaxTimeStamps(tablename);

            var currentimte = DateTime.Now;


            var dbcon = String.Format("DataSource={0}.db", dbproperties.DatabaseName);
            using (var connection = new SqliteConnection(dbcon))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    

                        foreach (string stock in stockdata)
                        {
                            var sepratedvalues = stock.Split(",");

                            if (minmax.Count == 0 || (minmax.Count > 0 && (minmax[0] > Convert.ToInt32(sepratedvalues[0]) || Convert.ToInt32(sepratedvalues[0]) > minmax[1])))
                            {
                                string query = "INSERT INTO {0} (timestamp, open, high, low, close, dateadded) VALUES ({1},{2},{3},{4},{5},'{6}' );";
                                query = Stringuery, dbproperties.TableName, sepratedvalues[0], sepratedvalues[1], sepratedvalues[2], sepratedvalues[3], sepratedvalues[4], currentimte.ToString());
                                // Console.WriteLine("new query is : {0}",newquery)

                                // Create New Command Object using the connection.
                                // Send the query to commandText property
                                command.CommandText = query;
                                //Open Connection

                                //Execute your command
                                command.ExecuteNonQuery();
                                //insert ito db

                                // bulkdata.append({"timestamp":sepratedvalues[0],"open":sepratedvalues[1],"high":sepratedvalues[2],"low":sepratedvalues[3],"close":sepratedvalues[4],"dateadded":currenttime})
                            }

                        }

                        
                        
                    

                }
                connection.Close();
            }







        }
        public void BulkSaveData(StockDataDetails sd)
        {
            Common cm = new Common();
            string tablename = this.dbproperties.TableName;
            Console.WriteLine("saving to {0}", tablename);

            var stockdata = sd.datarecieved.data;

            if (stockdata.Length == 0)
                return;

            var minmax = this.getMinMaxTimeStamps(tablename);

            var currentimte = DateTime.Now;


            var dbcon = String.Format("DataSource={0}.db", dbproperties.DatabaseName);
            using (var connection = new SqliteConnection(dbcon))
            {
                connection.Open();
                
                    using (var transaction = connection.BeginTransaction())
                    {

                        foreach (string stock in stockdata)
                        {
                            var sepratedvalues = stock.Split(",");

                            
                            if (minmax.Count == 0 || (minmax.Count > 0 && (minmax[0] > Convert.ToInt64(sepratedvalues[0]) || Convert.ToInt64(sepratedvalues[0]) > minmax[1])))
                            {
                                string query = "INSERT INTO {0} (timestamp, open, high, low, close, timestampdate, dateadded) VALUES ({1},{2},{3},{4},{5},'{6}','{7}' );";
                                query = String.Format(query, dbproperties.TableName, sepratedvalues[0], sepratedvalues[1], sepratedvalues[2], sepratedvalues[3], sepratedvalues[4],cm.UnixTimeStampToDateTime(sepratedvalues[0]),  currentimte.ToString());
                                // Console.WriteLine("new query is : {0}",newquery)

                                using(var command = connection.CreateCommand()){
                                    command.CommandText = query;
                                    //Open Connection

                                    //Execute your command
                                    command.ExecuteNonQuery();
                                }
                                // // Create New Command Object using the connection.
                                // var command = connection.CreateCommand();
                                // // Send the query to commandText property
                                
                                //insert ito db

                                // bulkdata.append({"timestamp":sepratedvalues[0],"open":sepratedvalues[1],"high":sepratedvalues[2],"low":sepratedvalues[3],"close":sepratedvalues[4],"dateadded":currenttime})
                            }

                        }

                        transaction.Commit();
                        
                    }

                
                connection.Close();
            }







        }

        public void CreateTables()
        {
            var dbcon = String.Format("DataSource={0}.db", dbproperties.DatabaseName);

            using (var connection = new SqliteConnection(dbcon))
            {
                string query = "CREATE TABLE IF NOT EXISTS {0} ("
                             + "historyid INTEGER PRIMARY KEY AUTOINCREMENT,"
                             + "timestamp INTEGER NOT NULL,"
                             + "open REAL NOT NULL, high REAL NOT NULL, low REAL NOT NULL, close REAL NOT NULL, timestampdate TEXT NOT NULL, dateadded TEXT NOT NULL)";

                query = String.Format(query, dbproperties.TableName);
                // Console.WriteLine("new query is : {0}",newquery)

                // Create New Command Object using the connection.
                var command = connection.CreateCommand();
                // Send the query to commandText property
                command.CommandText = query;
                //Open Connection
                connection.Open();
                //Execute your command
                command.ExecuteNonQuery();
            }
        }

        // public void CheckAndCreateTable(){
        //     DataSaver ds = new DataSaver();
        //     CreateTables();
        // }
    }
