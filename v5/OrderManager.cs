using System;
using System.Collections;
using System.Collections.Generic;

namespace v4
{
    
public class OrderManager{

        public List<Order> orderList = new List<Order>();
        public List<Signal> signalList = new List<Signal>();
        public void sendSignal(Signal signal){
        
        var signalexists = signalList.FindLast(x=>x.symbolName==signal.symbolName && x.positionType == signal.positionType && x.signalType == signal.signalType && x.expired==false);
        var oldsignals = signalList.FindAll(x=>x.symbolName==signal.symbolName && x.positionType == signal.positionType && x.signalType != signal.signalType);
        foreach(Signal sg in oldsignals){
            sg.expired=true;
        }
        if(signalexists==null)
        signalList.Add(signal);
        else{
            return;
        }

        
        Console.WriteLine("Recieved signal =======");
        Console.WriteLine("Symbol Name: {0}",signal.symbolName);
        Console.WriteLine("Time: {0}",signal.timestamp);
        Console.WriteLine("Position Type: {0}",signal.positionType);
        Console.WriteLine("Signal Type: {0}",signal.signalType);
        Console.WriteLine("Max Retracement: {0}",signal.maxretracepercent);
        
                            // converting signal to order here
                    // var lastorder = GetLastOrder(signal.symbolName);

                    // var order = new Order();
                    // order.Symbol=signal.symbolName;
                    // order.Entryprice=signal.allHigh;
                    // // order.sL=signal.maxretracepercent;
                    // // order.Profit = signal.allHigh-signal.maxretracepercent;
                    // order.positionType=PositionType.longposition;
                    // order.orderVeriety = OrderVeriety.regular;
                    // order.quantity=10;
                    // this.AddOrder(order);

            //in this strategy, we dont want to try mutliple trades
            // if(onlyonce==true && lastorder!=null && lastorder.orderstatus==OrderStatus.positionClosed)
            // break;

        }
        public void AddOrder(Order order){
            orderList.Add(order);
        }

        public void CheckOrder(string symbolName,OHLC oHLC){
            //check the orders recieved and do the needful as per the order properties
        }

        public void FeedData(string symbolName,OHLC oHLC){

                CheckOrder(symbolName,oHLC);
        }

        public Order GetLastOrder(string SymbolName){
            var lastorder = orderList.FindLast(x=>x.Symbol==SymbolName);
            return lastorder;
        }

        
    }

}