using System;
using System.Collections;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;   
using NiL.JS.Core;
using NiL.JS.BaseLibrary;
using NiL.JS.Extensions;


namespace v4
{
    
    public enum SignalType{
        early=1,
        abouttoform=2,
        onedge =3,
        formed=4
    }
    public enum PositionType{
        longposition=1,
        shortposition=2

    }

    public enum OrderVeriety{
        regular=1,
        bracketorder=2
    }

    public enum OrderStatus{
        EntryPending=1,
        EntryCompleted=2,
        ExitInitiated=3,
        ExitPending=4,
        ExitExecuted=5,
        positionClosed=6
    }

    public class Order{
        public string Symbol;
        public OrderVeriety orderVeriety;
        public PositionType positionType;
        public double Entryprice;
        public double Profit;
        public double sL;
        public OrderStatus orderstatus;
        public int quantity;
        

    }

    public class Signal{
        public string symbolName;
        public SignalType signalType;
        public double allHigh;
        public double allLow;
        public double maxretracepercent;
        public PositionType positionType;
        public Int64 timestamp;
        public bool expired=false;
    }

    
    class StrategyAnalyzer{

        public void HighBreakOutJS(AnalysisData data){
            var context = new Context();
             context.Eval("var concat = (a, b) => a + ', ' + b");

            var concatFunction = context.GetVariable("concat").As<Function>();

            Console.WriteLine(concatFunction.Call(new Arguments { "Hello", "World!" })); // Console: 'Hello, World!'
        }
        public void HighBreakOut(AnalysisData data){
            OrderManager orderManager=new OrderManager();
            double allHigh=0;
            double allLow=0;
            double maxretracepercent=0;
            double currentretracepercent=0;
            string lasttouched="";

            foreach(OHLC ohlcdata in  data.dataforanalysis){
                Console.WriteLine();
                Console.WriteLine("timstamp is : {0} ****************",ohlcdata.timestammp);
                
                // this should be if timestamnp!=9.15 and waitagain==1
                //  so that multiple targets can be calculated with in the same stock
                if(ohlcdata.timestammp==915){
                    allHigh=ohlcdata.high;
                    allLow=ohlcdata.low;
                }

                
                if(ohlcdata.high>allHigh)
                {
                    allHigh=ohlcdata.high;
                    maxretracepercent=0;
                    lasttouched="high";
                    
                }
                if(ohlcdata.low<allLow)
                {
                    allLow=ohlcdata.low;
                    maxretracepercent=0;
                    lasttouched="low";
                    
                }

                // Console.WriteLine("allHigh is : {0}",allHigh);
                // Console.WriteLine("allLow is : {0}",allLow);
                // Console.WriteLine("current high is : {0}",ohlcdata.high);

                

                if(ohlcdata.low>allLow && ohlcdata.high<allHigh){
                    if(lasttouched=="high"){
                        var retraced=allHigh-ohlcdata.high;
                        var totalvalue=allHigh-allLow;
                        currentretracepercent = (retraced/totalvalue)*100;
                        // if new retracement is higher, consider that
                        if (maxretracepercent<currentretracepercent){
                            maxretracepercent=currentretracepercent;
                        }

                        // Console.WriteLine("retraced is : {0}",retraced);
                        // Console.WriteLine("totalvalue is : {0}",totalvalue);
                        // Console.WriteLine("currentretracepercent is : {0}",currentretracepercent);
                        // Console.WriteLine("maxretracepercent is : {0}",maxretracepercent);
                    }
                    else if(lasttouched=="low"){
                        var retraced=ohlcdata.low-allLow;
                        var totalvalue=allHigh-allLow;
                        currentretracepercent = (retraced/totalvalue)*100;
                        // if new retracement is higher, consider that
                        if (maxretracepercent<currentretracepercent){
                            maxretracepercent=currentretracepercent;
                        }
                    }
                    
                }

                
                
                
                //send order
                if(lasttouched=="high" && maxretracepercent >0 && maxretracepercent <50 ){
                    Signal signal = new Signal();
                    signal.symbolName=data.SymbolName;
                    signal.allHigh=allHigh;
                    signal.maxretracepercent=maxretracepercent;
                    signal.positionType=PositionType.longposition;
                    signal.timestamp=ohlcdata.timestammp;

                    if(currentretracepercent<5 & currentretracepercent>=maxretracepercent){
                        signal.signalType=SignalType.early;
                    }
                    else if(currentretracepercent>5  & currentretracepercent>=maxretracepercent){
                        signal.signalType=SignalType.abouttoform;
                    }
                    if(currentretracepercent<5 & currentretracepercent<maxretracepercent){
                        signal.signalType=SignalType.onedge;
                    }
                    
                    
                    orderManager.sendSignal(signal);
                }
                else if(lasttouched=="low" && maxretracepercent >0 && maxretracepercent <50 ){
                    Signal signal = new Signal();
                    signal.symbolName=data.SymbolName;
                    signal.allLow=allLow;
                    signal.maxretracepercent=maxretracepercent;
                    signal.positionType=PositionType.shortposition;
                    signal.timestamp=ohlcdata.timestammp;

                    if(currentretracepercent<5 & currentretracepercent>=maxretracepercent){
                        signal.signalType=SignalType.early;
                    }
                    else if(currentretracepercent>30  & currentretracepercent>=maxretracepercent){
                        signal.signalType=SignalType.abouttoform;
                    }
                    if(currentretracepercent<5 & currentretracepercent<maxretracepercent){
                        signal.signalType=SignalType.onedge;
                    }

                    orderManager.sendSignal(signal);
                }
                // write something which can give you signals which are already crossed
                // like last high hit
                


                

                



                

                

                

                // //long retracement count
                // if(enteredprice==0&& ohlcdata.high<allHigh && highcount>1){
                //     longReady=1;

                //     // enteredprice = allHigh;
                //     // enteredtimestamp = ohlcdata.timestammp;
                //     // stoploss = ohlcdata.low; //temporary, keep modifying this, display the new stoploss
                //     // target = enteredprice+(enteredprice-stoploss);

                //     if(ohlcdata.low==0||ohlcdata.low<retrLow)
                //     retrLow=ohlcdata.low;

                //     retrLowCount+=1;
                // }

                // //long buy place


                // //exit
                // if(enteredprice>0 && ohlcdata.high>=target){
                //     exitedprice = ohlcdata.high;
                //     exitedtimestamp = ohlcdata.timestammp;
                //     waitagain=1;

                //     double profit = exitedprice-enteredprice;
                //     Console.WriteLine("Entered price is {0} at timestamp {1}",enteredprice,enteredtimestamp);
                //     Console.WriteLine("Exited price is {0} at timestamp {1}",exitedprice,exitedtimestamp);
                //     Console.WriteLine("Actual target was {0}",target);
                //     Console.WriteLine("Total profit is {0}",profit);
                // }

                // //stoploss hit
                // if(enteredprice>0 && ohlcdata.low<=stoploss){
                //     exitedprice=stoploss;
                //     exitedtimestamp = ohlcdata.timestammp;

                //     double profit = exitedprice-enteredprice;
                //     Console.WriteLine("Entered price is {0} at timestamp {1}",enteredprice,enteredtimestamp);
                //     Console.WriteLine("Exited price is {0} at timestamp {1}",exitedprice,exitedtimestamp);
                //     Console.WriteLine("Actual target was {0}",target);
                //     Console.WriteLine("Total profit is {0}",profit);
                // }

                //  handle stop loss
                //  and reversal (bearish trends)
                //  and pure bearish trends
                

                
                
                

            }

            
        }

        public void Start(AnalysisData data,string StrategyName,string AnalysisType){

            HighBreakOut(data);
            
            
        }

    }
}