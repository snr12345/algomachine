using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json.Linq;


namespace v4
{
    public class UpStoxUtil
    {
        string upstox_authurl = "https://api.upstox.com/index/dialog/authorize?apiKey={0}&redirect_uri={1}&response_type=code";
        string upstox_tokenurl = "https://api.upstox.com/index/oauth/token/";
        string upstox_tokenpayload = "code={0}&redirect_uri={1}&grant_type=authorization_code";
        // string upstoxk_dataurl = "https://api.upstox.com/{datatype}/{exchangename}/{symbol}/{intervaltype}";
        string upstoxk_dataurl = "https://api.upstox.com/{0}/{1}/{2}/{3}?start_date={4}&end_date={5}";
        string access_token = default(string);
        string lastauthcode = default(string);

        string apiKey = default(string);
        string apiSecret = default(string);
        string redirect_uri = default(string);

        public UpStoxUtil(string apikey, string apiSecret, string redirect_uri)
        {
            this.apiKey = apikey;
            this.apiSecret = apiSecret;
            this.redirect_uri = redirect_uri;
        }

        public string getloginurl()
        {
            var loginurl = String.Format(upstox_authurl, this.apiKey, this.redirect_uri);
            return loginurl;
        }

        public string getToken(string authcode)
        {
            access_token = string.Empty;
            this.lastauthcode = authcode;

            // WebRequest request = WebRequest.Create("https://api.upstox.com/index/oauth/token/");
            // request.Method = "POST";

            // request.Headers.Add("Content-Type", "application/json");
            // request.Headers.Add("x-api-key", apiKey);

            // request.Headers.Add("undefined", "code="+authcode+"&redirect_uri=http%3A%2F%2F127.0.0.1&grant_type=authorization_code");
            // WebResponse response = request.GetResponse();


            //            Console.WriteLine(((HttpWebResponse)response).StatusDescription);

            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://api.upstox.com/index/oauth/token"))
                {
                    request.Headers.TryAddWithoutValidation("x-api-key", apiKey);

                    var base64authorization = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{apiKey}:{apiSecret}"));
                    request.Headers.TryAddWithoutValidation("Authorization", $"Basic {base64authorization}");

                    request.Content = new StringContent("{\"code\" : \""+authcode+"\", \"grant_type\" : \"authorization_code\", \"redirect_uri\" : \""+redirect_uri+"\"}", Encoding.UTF8, "application/json");
                    
                    var response = httpClient.SendAsync(request);

                    if(response.Result.StatusCode==HttpStatusCode.OK){
                        
                        var responsetext = response.Result.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("success {0}",responsetext);
                    
                        var totalresponse = JObject.Parse(responsetext);
                        this.access_token = totalresponse["access_token"].ToString();
                        



                    }
                    else{
                        Console.WriteLine("failed: {0}",response.Result.ReasonPhrase);
                    }

                    

                    
                    
                }
            }


            return access_token;
        }

        public void setToken(string access_token)
        {
            this.access_token = access_token;
        }

        public string getStockData(string datatype,string exchangename,string symbol,string intervaltype,string startdate,string enddate){

                string responsedata = String.Empty;
                string urlfordata = String.Format(upstoxk_dataurl,datatype,exchangename,symbol,intervaltype,startdate,enddate);



                using (var httpClient = new HttpClient())
                {
                    using (var request = new HttpRequestMessage(new HttpMethod("GET"), urlfordata))
                    {
                        request.Headers.TryAddWithoutValidation("Authorization", "Bearer "+this.access_token);
                        request.Headers.TryAddWithoutValidation("x-api-key", apiKey); 

                        var response = httpClient.SendAsync(request);

                        if (response.Result.StatusCode == HttpStatusCode.OK)
                        {

                             responsedata = response.Result.Content.ReadAsStringAsync().Result;
                            

                        }
                        else
                        {
                            Console.WriteLine("failed: {0}", response.Result.ReasonPhrase);
                        }
                    }
                }

                return responsedata;
        }

    }

}
