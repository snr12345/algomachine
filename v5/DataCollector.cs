using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;



namespace v4
{
    class DataCollector
    {

        string apiKey = "oeACc9ejKG1C1A7Ju6Fj97fabaV11ZgQ8sLA24Bk";
        string apiSecret = "g9sm7kctwk";
        string redirect_uri = "http://127.0.0.1";

        UpStoxUtil datautil;

        public DataCollector()
        {
            setupBroker();
        }
        public void OpenBrowser(string url)
        {
            try
            {
                Process.Start(url);
            }
            catch
            {
                // hack because of this: https://github.com/dotnet/corefx/issues/10361
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    url = url.Replace("&", "^&");
                    Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    Process.Start("xdg-open", url);
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    Process.Start("open", url);
                }
                else
                {
                    throw;
                }
            }
        }
        public void setupBroker()
        {
            UpStoxUtil ut = new UpStoxUtil(apiKey, apiSecret, redirect_uri);
            datautil = ut;

            Console.WriteLine(" 1. Start from begening \n 2. Enter token manually \n");

            string optionid = Console.ReadLine();

            if (Convert.ToInt32(optionid) == 1)
            {

                string url = this.datautil.getloginurl();
                // Console.WriteLine("Enter this url in browser : {0}",url);
                OpenBrowser(url);

                // webbrowser.open_new_tab(url)

                Console.WriteLine("Enter the code you have recieved:");

                string codrecieved = Console.ReadLine();

                // # add the code here
                string token = this.datautil.getToken(codrecieved);

                Console.WriteLine("Token has been recieved and set, you can make a note of this token: {0}", token);
            }
            else
            {
                Console.WriteLine("Enter the token you may have with you:");
                string token = Console.ReadLine();
                this.datautil.setToken(token);
            }



        }


        public StockDataDetails getStockData(string datatype, string selectedexchangename, string symbol, string intervalType, DateTime startdate, DateTime enddate)
        {

            Console.WriteLine("collecting the data from util");
            var strstartdate = startdate.ToString("dd-MM-yyyy");
            var strenddate = enddate.ToString("dd-MM-yyyy");
            string datarecived = this.datautil.getStockData(datatype, selectedexchangename, symbol, intervalType, strstartdate, strenddate);

            StockResponse data = JsonConvert.DeserializeObject<StockResponse>(datarecived);

            if (data.code != "200")
            {
                Console.WriteLine("Error :{0}, please correct your token/dates and try again.", data.message);
                return null;
            }
            else
            {
                StockDataDetails sd = new StockDataDetails();
                sd.ExchangeName = selectedexchangename;
                sd.SymbolName = symbol;
                sd.datarecieved = data;
                return sd;
            }


        }


    }

}
