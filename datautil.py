import json
import datetime
import requests
from upstox_api.api import *

class puller:
    def __init__(self):
        self.api_key='oeACc9ejKG1C1A7Ju6Fj97fabaV11ZgQ8sLA24Bk'
        self.api_secret='g9sm7kctwk'
        self.redirect_uri='http://127.0.0.1'
        self.access_token=None
        self.code=None
        self.upstoxobj=None

        upstoxsession = Session (self.api_key)
        upstoxsession.set_redirect_uri (self.redirect_uri)
        upstoxsession.set_api_secret (self.api_secret)
        self.upstoxsession=upstoxsession

    def getLoginURLFromUpstox(self):
        return self.upstoxsession.get_login_url()


    def setUpstoxToken(self,code):
        self.code=code
        self.upstoxsession.set_code (code)
        self.access_token = self.upstoxsession.retrieve_access_token()
        self.upstoxobj = Upstox (self.api_key, self.access_token)
        # to search in future
        self.upstoxobj.get_master_contract('NSE_EQ')
        

    def getProfile(self):
        if self.code==None:
            print('Code is not set yet')
            return
        print (self.upstoxobj.get_profile())

    def getHistoricalData(self,Symbol,Interval,StartTime,EndTime):
        actualsymbol=self.upstoxobj.get_instrument_by_symbol('NSE_EQ', Symbol)
        result = self.upstoxobj.get_ohlc(actualsymbol, Interval, StartTime, EndTime)
        return result

