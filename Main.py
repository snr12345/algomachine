import backfiller
reload(backfiller)
import datasaver
reload(datasaver)
import datacollector
reload(datacollector)
import lookups
reload(lookups)
import datetime
import time
import testmodule
reload(testmodule)
import datautil
reload(datautil)
import subprocess

# variables
selectedintervalType=None
selectedcandleperiod=None
starttime=None
endtime=None

# user settings
selectedfilltype="history"
selectedintervalType = "1min"
selectedrangenumber = 200
selectedstarttime = '25-09-2019'
selectedendtime = '26-09-2019'
selectedSymbol="HDFCBANK"

# database settings
intervalType = lookups.intervalTypes[selectedintervalType]

if selectedfilltype=="backfill":
    # backfill options
    intervalTypemilliseconds=lookups.intervalTypeMS[selectedintervalType]
    backfillmillisecnods = selectedrangenumber*intervalTypemilliseconds
    print(backfillmillisecnods)
    timenow=datetime.datetime.now()
    
    starttime = timenow-datetime.timedelta(milliseconds=backfillmillisecnods)
    endtime = timenow
else:
    #historicdatainsert
    starttime = datetime.datetime.strptime(selectedstarttime, '%d-%m-%Y').date()
    endtime = datetime.datetime.strptime(selectedendtime, '%d-%m-%Y').date()


print(starttime)
print(endtime)



pul = datautil.puller()
subprocess.call()
pul.getLoginURLFromUpstox()
pul.setUpstoxToken("fc71723cb4e9a0bee23efb8efdb7fbca2c3b9a1c")
pul.getProfile()
data = pul.getHistoricalData(selectedSymbol,intervalType,starttime,endtime)