import datetime

def datetostr(dateval):
    strvalue = dateval.strftime("%d-%m-%Y")
    return strvalue

def strtodate(strval):
    datevalue = datetime.datetime.strptime(strval,"%d-%m-%Y")
    return datevalue

def SplitDays(startdate,enddate):
    startdate_Date = strtodate(startdate)
    enddate_Date = strtodate(enddate)
    delta = enddate_Date-startdate_Date
    weeksneeded = int(delta.days/7)
    daysleft = delta.days%7

    rangelist=[]

    if weeksneeded==0:
        rangelist.append({'startdate':startdate,'enddate':enddate})
    else:
        laststartdate=None
        lastenddate=None
        currentstartdate=None
        currentenddate=None
        
        for i in range(weeksneeded):
            

            if(i==0):
                currentstartdate = startdate_Date
            else:
                currentstartdate = lastenddate+datetime.timedelta(days=1)

            currentenddate = currentstartdate+datetime.timedelta(days=6)
            lastenddate=currentenddate

            

            rangelist.append({'startdate':datetostr(currentstartdate),'enddate':datetostr(currentenddate)})
        # once loop is done or weeks added, now add remaining days
        currentstartdate = lastenddate+datetime.timedelta(days=1)
        currentenddate = currentstartdate+datetime.timedelta(days=daysleft)
        rangelist.append({'startdate':datetostr(currentstartdate),'enddate':datetostr(currentenddate)})


    return rangelist