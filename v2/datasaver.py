import json
import datetime
import requests
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy import Table, Column, Integer,Float,DateTime, String, MetaData, text, select
from sqlalchemy.sql import func



datacollected={}



class datasaver:
    def __init__(self):
        self.engine = create_engine('sqlite:///myalgotest.db', echo = True)
        self.meta = MetaData()
        self.tables={}
        
        
    def createobjects(self,tablename):
        
        self.conn = self.engine.connect()
        # tablename = "{0}_{1}".format(symbolname,self.runidentifer)
        # t = text("drop table if exists "+tablename)
        # result = self.conn.execute(t)
        

        self.tables[tablename] = Table(
            tablename, self.meta, 
            Column('historyid', Integer, primary_key = True), 
            Column('timestamp', String), 
            Column('open', Float), 
            Column('high', Float), 
            Column('low', Float), 
            Column('close', Float),
            Column('dateadded',DateTime)
            )
        self.meta.create_all(self.engine)

        

# selectedexchangetype,symbol,intervalType

    def getMinMaxTimeStamps(self,tablename):
        table = self.tables[tablename]
        result = self.conn.execute(select([func.min(table.c.timestamp),func.max(table.c.timestamp)]))
        minmax= result.fetchall()
        vals = {"min":minmax[0][0],"max":minmax[0][1]}
        return vals


    def savetodb(self,datacollected,tablename):
        
        
        table = self.tables[tablename]

        
        stockdata = datacollected["datarecieved"]["data"]
        
        minmax = self.getMinMaxTimeStamps(tablename)
        
        bulkdata = []
        currenttime=datetime.datetime.now()

        if(minmax["min"]!=None and minmax["max"]!=None):
            for stock in stockdata:
                sepratedvalues = stock.split(",")

                if (int(minmax["min"])>int(sepratedvalues[0])) or (int(sepratedvalues[0])>int(minmax["max"])):
                    bulkdata.append({"timestamp":sepratedvalues[0],"open":sepratedvalues[1],"high":sepratedvalues[2],"low":sepratedvalues[3],"close":sepratedvalues[4],"dateadded":currenttime})

        else:
            # duplicate code added just to improve the performance
            for stock in stockdata:
                sepratedvalues = stock.split(",")
                bulkdata.append({"timestamp":sepratedvalues[0],"open":sepratedvalues[1],"high":sepratedvalues[2],"low":sepratedvalues[3],"close":sepratedvalues[4],"dateadded":currenttime})

        
        if(len(bulkdata)!=0):
            self.conn.execute(table.insert(),bulkdata)
        # ins = table.insert().values(close=1.56,exchangename="NSE",high=1.23,low=8.89,open=2.34,symbol="TCS",timestamp=678678585)
        # # print(ins)
        # self.conn.execute(ins)

        
    