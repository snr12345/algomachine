import json
import datetime
import requests

s=None
access_token=None

upstox_authurl = "https://api.upstox.com/index/dialog/authorize?apiKey={apiKey}&redirect_uri={redirect_uri}&response_type=code"
upstox_tokenurl ="https://api.upstox.com/index/oauth/token/"
upstox_tokenpayload="code={code}&redirect_uri={redirect_uri}&grant_type=authorization_code"
upstoxk_dataurl="https://api.upstox.com/{datatype}/{exchangename}/{symbol}/{intervaltype}"

class upstoxutil:
    def __init__(self,apikey,apiSecret,redirect_uri):
        self.apiKey=apikey
        self.apiSecret=apiSecret
        self.redirect_uri=redirect_uri
        
    def getloginurl(self):
        loginurl = upstox_authurl.format(apiKey=self.apiKey,redirect_uri=self.redirect_uri)
        return loginurl
    
    # only one token per code
    def getToken(self,authcode):
        access_token=None
        self.lastauthcode=authcode
        payload = upstox_tokenpayload.format(code=self.lastauthcode,redirect_uri=self.redirect_uri)
        headers = {
            'x-api-key': self.apiKey,
            'Content-Type': "application/x-www-form-urlencoded"
            }

        response = requests.request("POST", upstox_tokenurl, data=payload, headers=headers, auth=(self.apiKey,self.apiSecret))
        restext = response.text

        res = json.loads(restext)
        
        if("code" in res and res["code"]==401):
            print(res["message"],"access_token can not be set")
        elif("access_token" in res):
            access_token=res["access_token"]
            self.access_token=access_token

        return access_token

    def setToken(self,access_token):
        self.access_token=access_token

    def getData(self,datatype,exchangename,symbol,intervaltype,startdate,enddate):        
        urlfordata = upstoxk_dataurl.format(datatype=datatype,exchangename=exchangename,symbol=symbol,intervaltype=intervaltype)

        querystring = {"end_date":enddate,"start_date":startdate}

        authinfo = "Bearer {token}".format(token=self.access_token)

        headers = {
        'x-api-key': self.apiKey,
        'authorization':authinfo
        }

        
        response = requests.request("GET", urlfordata, headers=headers, params=querystring)

        resdata = response.text

        

        return resdata