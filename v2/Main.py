from importlib import reload


import backfiller
reload(backfiller)

import datasaver
reload(datasaver)

import datacollector
reload(datacollector)

import lookups
reload(lookups)

import common
reload(common)

import datetime
import time

import json

# variables
selectedintervalType=None
selectedcandleperiod=None
startdate=None
enddate=None

# user settings
selectedfilltype="bystartdate"
selecteddatatype="historical"
selectedexchangename="nse_eq"
selectedintervalTypes = ["1","5","15","60"]
selectedstartdate = '09-08-2019'
selectedSymbols=["HDFCBANK","TCS","ITC"]

# database settings


if selectedfilltype=="bystartdate":
    startdate = selectedstartdate

    enddate=datetime.datetime.now()
    enddate = common.datetostr(enddate)

elif selectedfilltype=="bynumber":
    # getting data based on number of candles
    # backfill options
    intervalTypemilliseconds=lookups.intervalTypeMS[selectedintervalType]
    backfillmillisecnods = selectedrangenumber*intervalTypemilliseconds
    timenow=datetime.datetime.now()
    
    startdate = timenow-datetime.timedelta(milliseconds=backfillmillisecnods)
    enddate = timenow

    startdate = common.datetostr(startdate)
    enddate = common.datetostr(enddate)
    

            

    

print(startdate)
print(enddate)



dc = datacollector.datacollector()
ds = datasaver.datasaver()

def StartBackFilling(symbol,intervalType,dateranges):
    def datapulled(data):
        # print('data recieved')
        # print(json.dumps(data,indent=4))
        ds.savetodb(data,tablename)

    
    tablename="{0}_{1}_{2}_{3}".format(symbol,selecteddatatype,selectedexchangename,intervalType)
    ds.createobjects(tablename)
    
    for daterange in dateranges:
        data = dc.getdataBySymbol(selecteddatatype,selectedexchangename,symbol,intervalType,daterange["startdate"],daterange["enddate"],datapulled)

for symbol in selectedSymbols:
    starttime = time.time()
    print('starting backfilling..')
    dateranges = common.SplitDays(startdate,enddate)
    for intervalType in selectedintervalTypes:
        StartBackFilling(symbol,intervalType,dateranges)
    endtime = time.time()

# datatype_exchangename_symbol_intervaltype
# pul = datautil.puller()
# subprocess.call()
# pul.getLoginURLFromUpstox()
# pul.setUpstoxToken("fc71723cb4e9a0bee23efb8efdb7fbca2c3b9a1c")
# pul.getProfile()
# data = pul.getHistoricalData(selectedSymbol,intervalType,startdate,enddate)