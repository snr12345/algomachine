import requests

url = "https://api.upstox.com/historical/nse_eq/TCS/1"

querystring = {"end_date":"26-09-2019","start_date":"25-09-2019"}

headers = {
    'x-api-key': "oeACc9ejKG1C1A7Ju6Fj97fabaV11ZgQ8sLA24Bk",
    'Authorization': "Bearer 7251bd2dfbd627307187f501e4664cfa76935d08",
    'User-Agent': "PostmanRuntime/7.17.1",
    'Accept': "*/*",
    'Cache-Control': "no-cache",
    'Postman-Token': "f1d7b6a6-871a-4216-a781-aa7f89a42b06,af637160-e623-40a2-9ad2-d7f8d06661ca",
    'Host': "api.upstox.com",
    'Accept-Encoding': "gzip, deflate",
    'Cookie': "sid=s%3A6NEm6wMMVEmQwZM5-AAXCykQ8bFvql_c.PNNeI1ksssY%2BIFQuFfCPJ47sCojFsvD99mSvLlduyN4",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
    }

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)