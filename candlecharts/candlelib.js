

function CandleChart(){
    
    var canvasMain;
    var canvasLines;
    var canvasSignals;
    var canvasVals;
    var ctxMain;
    var ctxLines;
    var ctxSignals;
    var ctxVals;
    var SymbolName;
    
    var SignalData;
    var IntervalType;
    var onemin = 60000;
    var onehour = onemin*60;
    var oneday = onehour*24;
    var oneweek = oneday*7;
    var onemonth = oneday*30;
    var oneyear = onemonth*12;
    
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var PricesForY =[];
    var minprice=10;
    var maxprice=50;
    var mindate;
    var maxdate;
    var totalYLinesLimit = 14;
    var candlewidth;
    var canldeWidthWithMargin;
    // var notes=[10,11,12,13,14,15,16,17,18,19];
    // var alldata = JSON.parse(decodeURI("<%= OHLCData %>"));
    
    var OHLCData;
    // var dataforpaste='{"OHLCData":[{"TimeStamp":1547058600000,"Open":24.29,"High":41.43,"Low":15.71,"Close":35.71},{"TimeStamp":1547058600000,"Open":12,"High":14,"Low":11.8,"Close":13},{"TimeStamp":1547058600000,"Open":14,"High":20,"Low":12.1,"Close":13.5},{"TimeStamp":1547058600000,"Open":15,"High":18,"Low":14,"Close":17},{"TimeStamp":1547058600000,"Open":24.29,"High":41.43,"Low":15.71,"Close":35.71},{"TimeStamp":1547058600000,"Open":12,"High":14,"Low":11.8,"Close":13},{"TimeStamp":1547058600000,"Open":14,"High":20,"Low":12.1,"Close":13.5},{"TimeStamp":1547058600000,"Open":15,"High":18,"Low":14,"Close":17},{"TimeStamp":1547058600000,"Open":24.29,"High":41.43,"Low":15.71,"Close":35.71},{"TimeStamp":1547058600000,"Open":12,"High":14,"Low":11.8,"Close":13},{"TimeStamp":1547058600000,"Open":14,"High":20,"Low":12.1,"Close":13.5},{"TimeStamp":1547058600000,"Open":15,"High":18,"Low":14,"Close":17},{"TimeStamp":1547058600000,"Open":24.29,"High":41.43,"Low":15.71,"Close":35.71},{"TimeStamp":1547058600000,"Open":12,"High":14,"Low":11.8,"Close":13},{"TimeStamp":1547058600000,"Open":14,"High":20,"Low":12.1,"Close":13.5},{"TimeStamp":1547058600000,"Open":15,"High":18,"Low":14,"Close":17}],"SignalData":{"SignalType":"early","TimeStamp":"1547058600001","AllHigh":"30","AllLow":"10","PositionType":"long"},"IntervalType":"1","SymbolName":"TCS"}';
    // var OHLCData='[{"TimeStamp":1547058600000,"Open":24.29,"High":41.43,"Low":15.71,"Close":35.71},{"TimeStamp":1547058600000,"Open":12,"High":14,"Low":11.8,"Close":13},{"TimeStamp":1547058600000,"Open":14,"High":20,"Low":12.1,"Close":13.5},{"TimeStamp":1547058600000,"Open":15,"High":18,"Low":14,"Close":17},{"TimeStamp":1547058600000,"Open":24.29,"High":41.43,"Low":15.71,"Close":35.71},{"TimeStamp":1547058600000,"Open":12,"High":14,"Low":11.8,"Close":13},{"TimeStamp":1547058600000,"Open":14,"High":20,"Low":12.1,"Close":13.5},{"TimeStamp":1547058600000,"Open":15,"High":18,"Low":14,"Close":17},{"TimeStamp":1547058600000,"Open":24.29,"High":41.43,"Low":15.71,"Close":35.71},{"TimeStamp":1547058600000,"Open":12,"High":14,"Low":11.8,"Close":13},{"TimeStamp":1547058600000,"Open":14,"High":20,"Low":12.1,"Close":13.5},{"TimeStamp":1547058600000,"Open":15,"High":18,"Low":14,"Close":17},{"TimeStamp":1547058600000,"Open":24.29,"High":41.43,"Low":15.71,"Close":35.71},{"TimeStamp":1547058600000,"Open":12,"High":14,"Low":11.8,"Close":13},{"TimeStamp":1547058600000,"Open":14,"High":20,"Low":12.1,"Close":13.5},{"TimeStamp":1547058600000,"Open":15,"High":18,"Low":14,"Close":17}]';
    // var OHLCData =[
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:5,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4},
    //     {TimeStamp:1547058600000,Open:2,High:6,Low:1,Close:4}
       
    //     ];
    // var OHLCData =[
    //     {TimeStamp:1547058600000,Open:24.29,High:41.43,Low:15.71,Close:35.71},
    //     {TimeStamp:1547058600000,Open:12,High:14,Low:11.8,Close:13},
    //     {TimeStamp:1547058600000,Open:14,High:20,Low:12.1,Close:13.5},
    //     {TimeStamp:1547058600000,Open:15,High:18,Low:14,Close:17},
    //     {TimeStamp:1547058600000,Open:24.29,High:41.43,Low:15.71,Close:35.71},
    //     {TimeStamp:1547058600000,Open:12,High:14,Low:11.8,Close:13},
    //     {TimeStamp:1547058600000,Open:14,High:20,Low:12.1,Close:13.5},
    //     {TimeStamp:1547058600000,Open:15,High:18,Low:14,Close:17},
    //     {TimeStamp:1547058600000,Open:24.29,High:41.43,Low:15.71,Close:35.71},
    //     {TimeStamp:1547058600000,Open:12,High:14,Low:11.8,Close:13},
    //     {TimeStamp:1547058600000,Open:14,High:20,Low:12.1,Close:13.5},
    //     {TimeStamp:1547058600000,Open:15,High:18,Low:14,Close:17},
    //     {TimeStamp:1547058600000,Open:24.29,High:41.43,Low:15.71,Close:35.71},
    //     {TimeStamp:1547058600000,Open:12,High:14,Low:11.8,Close:13},
    //     {TimeStamp:1547058600000,Open:14,High:20,Low:12.1,Close:13.5},
    //     {TimeStamp:1547058600000,Open:15,High:18,Low:14,Close:17}
       
    //     ];
    
    var totalpointslimit;
        var canvasHeight=550;
        var canvasWidth=1050;
        
        function cx(val)
        {
            var ret = (canvasWidth/100)*val;
            return ret;
        }
        function cy(val)
        {
            var ret = (canvasHeight/100)*val;
            return ret;
        }
        function rcx(val)
        {
            var ret=val/(canvasWidth/100);
            return ret;
        }
        function rcy(val)
        {
            var ret=val/(canvasHeight/100);
            return ret;
        }
        function display(val){
            var extest = document.getElementById('display').innerHTML;
            document.getElementById('display').innerHTML=extest+"<br>"+val;
        }
        function drawCanvas(){
            
            ctxMain.canvas.style.backgroundColor = "#606060";
            //ctxMain.canvas.style.display = "none";
            //ctxLines.canvas.style.display = "none";
            //ctxSignals.canvas.style.backgroundColor = "#606060";
            
            
            // ctxMain.canvas.style.backgroundColor="#262626";
            canvasheight = $(ctxMain.canvas).parent().height();
            canvaswidth = $(ctxMain.canvas).parent().width();
            // console.log($(ctxMain.canvas).parent())
            ctxMain.canvas.height=canvasheight;
            ctxMain.canvas.width = canvaswidth;
            if (ctxLines.canvas.height != canvasheight) {
                ctxLines.canvas.height = canvasheight;
                ctxLines.canvas.width = canvaswidth;    
            }
            
            ctxSignals.canvas.height = canvasheight;
            ctxSignals.canvas.width = canvaswidth;
            
            ctxMain.moveTo(cx(90),cy(10));
            ctxMain.lineTo(cx(90),cy(90));
            ctxMain.lineTo(cx(10),cy(90));
            ctxMain.lineTo(cx(10),cy(10));
            ctxMain.strokeStyle="white";
            ctxMain.stroke();
            
            
            
            
            
        }
        
        function drawPriceLines(){
            
            
            //drawPriceLine(eachswaraPercentage);
            PricesForY.forEach(function(item,index){
                var pospixel = getypositionMarks(index);
                drawPriceLine(pospixel,item);
            })
            
            
            
        }
        function drawPriceLine(num,note){
            //display(num);
            ctxMain.beginPath();
            ctxMain.moveTo(cx(90),cy(num));
            ctxMain.lineTo(cx(10),cy(num));
            ctxMain.strokeStyle="grey";
            ctxMain.stroke();
            ctxMain.beginPath();
            ctxMain.font = "90% Georgia";
            ctxMain.fillStyle="white"
            ctxMain.fillText(" "+note, cx(90), cy(num+0.8));
            
            ctxMain.stroke();
        }
        
        
        function getypositionMarks(number)
        {
            var perHFactor = 80/totalYLinesLimit;
            // var skipH = 100*perHFactor;
            var pos = (perHFactor*number);
            return 90-pos;
        }
        function getypositionByPrice(price){
            var inputprice=price-minprice;
            var diffval = maxprice-minprice;
            var eachdiff = diffval/totalYLinesLimit;
            var forprice = inputprice/eachdiff;
            var perHFactor = 80/totalYLinesLimit;
            var pos =forprice*perHFactor
            x = 90-pos;
            return x;
            //returning in percent
        }
        // can not take percent inputs, so should provide direct x co ordinate
        function getyPriceByPosition(yposition){
            var YinPercent = rcy(yposition);
            var diffval = maxprice-minprice;
            var eachdiff = diffval/totalYLinesLimit;
            var perHFactor = 80/totalYLinesLimit;
            var pos = 90-YinPercent;
            var forprice=pos/perHFactor;
            var inputprice = forprice*eachdiff;
            var price = inputprice+minprice;
            return price;
        }
        function getxIndexByposition(xposition)
        {
            //adjustment
            // var XinPercent=xposition;
            var perWFactor = 80/(totalpointslimit+1);
            var pos = 90-xposition;
            
            var index = pos/perWFactor;
            return index;
        }
        function getxpositionByIndex(index)
        {
            var perWFactor = 80/(totalpointslimit+1);
            var pos = (perWFactor*index);
            return 90-pos;
        }
        function getyposition(pricedata)
        {
            
            positions={}
            positions.Open=getypositionByPrice(pricedata.Open);
            positions.High=getypositionByPrice(pricedata.High);
            positions.Low=getypositionByPrice(pricedata.Low);
            positions.Close=getypositionByPrice(pricedata.Close);
            
            return positions;
        }
        
        
        
        var pixelpositions=[];
        function preparePixels(){
            var revindex = OHLCData.length;
            pixelpositions=[];
            OHLCData.forEach(function(item,index){
                // console.log(item)
                var positionyval = getyposition(item);
                var positionxval = getxpositionByIndex(revindex-index);
                pixelpositions.push({x:positionxval,y:positionyval});
            })
        }
        function drawCandles(){
            preparePixels();
            
            pixelpositions.forEach(function(item,index){
                
                ctxMain.beginPath();
                ctxMain.moveTo(cx(item.x),cy(item.y.Low));
                ctxMain.lineTo(cx(item.x),cy(item.y.High));
                ctxMain.strokeStyle="white";
                ctxMain.stroke();
                
                ctxMain.beginPath();
                ctxMain.rect(cx(item.x)-cx(candlewidth/2) , cy(item.y.Close), cx(candlewidth), cy(-(item.y.Close-item.y.Open)));
                ctxMain.stroke();
                if(item.y.Close<item.y.Open)
                ctxMain.fillStyle="green";
                else{
                ctxMain.fillStyle="red"
                }
                ctxMain.fill();
                
                var curdate = new Date(OHLCData[index].TimeStamp);
                labels = XLabelSelection(curdate);
                ctxMain.beginPath();
                ctxMain.moveTo(cx(item.x),cy(0.8))
                var fontsize = candlewidth*2
                 ctxMain.font= fontsize+"px Arial";
                // ctxMain.+"% Georgia";
                ctxMain.fillStyle="white"
                ctxMain.textAlign = "center";
                ctxMain.fillText(labels.first , cx(item.x), cy(95));
                ctxMain.fillText(labels.second , cx(item.x), cy(98));
                ctxMain.stroke();
                
            })
            
            
        }
        function drawHeader(){
                var fromdt = mindate.toLocaleDateString()+" "+mindate.toLocaleTimeString();
                var todt = mindate.toLocaleDateString()+" "+mindate.toLocaleTimeString();
                ctxMain.beginPath();
                // ctxMain.moveTo(cx(50),cy(5))
                ctxMain.font= "30px Arial";
                ctxMain.fillStyle="white"
                ctxMain.textAlign = "center";
                ctxMain.fillText(SymbolName+"'s data from "+fromdt+" to "+todt, cx(50), cy(7));
                // ctxMain.fillText(labels.second , cx(item.x), cy(98));
                ctxMain.stroke();
        }
    function displaysignal(positionx, positiony, signal){
            //short
            var PositionType = signal.PositionType;
        var SignalType = signal.SignalType;
        var EnterAt = signal.EnterAt;
        var ExitAt = signal.ExitAt;
        var ProfitPercent = signal.ProfitPercent;
        //ctxSignals.clearRect(0, 0, canvasSignals.width, canvasSignals.height);
            if(PositionType=="short")
            {
            
            // console.log(positionx,positiony)
                ctxSignals.beginPath();
                ctxSignals.globalAlpha = 0.5;
                ctxSignals.moveTo(cx(positionx-2),cy(positiony.High-5));
                ctxSignals.lineTo(cx(positionx+2),cy(positiony.High-5));
                ctxSignals.lineTo(cx(positionx),cy(positiony.High-1));
                ctxSignals.lineTo(cx(positionx-2),cy(positiony.High-5));
                ctxSignals.rect(cx(positionx-0.5), cy(positiony.High-12), cx(1), cy(7));
                ctxSignals.stroke();
                ctxSignals.fillStyle="red";
                ctxSignals.fill();
                ctxSignals.globalAlpha = 1;
                ctxSignals.fillStyle="white";
                ctxSignals.fillText(SignalType, cx(positionx + 2), cy(positiony.High - 5))
                ctxSignals.fillStyle = "yellow";
                //ctxSignals.fillText("BY: " + EnterAt, cx(positionx + 2), cy(positiony.High - 11))
                //ctxSignals.fillText("TG: " + ExitAt, cx(positionx + 2), cy(positiony.High - 9))
                ctxSignals.fillText("PT: " + ProfitPercent, cx(positionx + 1), cy(positiony.High - 7))
            }
            else{
                // console.log(positionx,positiony)
                ctxSignals.beginPath();
                ctxSignals.globalAlpha = 0.5;
                ctxSignals.moveTo(cx(positionx-2),cy(positiony.Low+5));
                ctxSignals.lineTo(cx(positionx+2),cy(positiony.Low+5));
                ctxSignals.lineTo(cx(positionx),cy(positiony.Low+1));
                ctxSignals.lineTo(cx(positionx-2),cy(positiony.Low+5));
                ctxSignals.rect(cx(positionx-0.5), cy(positiony.Low+5), cx(1), cy(12));
                ctxSignals.stroke();
                ctxSignals.fillStyle="green";
                ctxSignals.fill();
                ctxSignals.globalAlpha = 1;
                ctxSignals.fillStyle = "white";
                ctxSignals.fillText(SignalType, cx(positionx + 2), cy(positiony.Low + 5))
                ctxSignals.fillStyle = "yellow";
                
                //ctxSignals.fillText("BY: "+EnterAt, cx(positionx + 2), cy(positiony.Low + 7))
                //ctxSignals.fillText("TG: " + ExitAt, cx(positionx + 2), cy(positiony.Low + 9))
                ctxSignals.fillText("PT: " + ProfitPercent, cx(positionx + 1), cy(positiony.Low + 11))
            }
        }
        function showSignalData(){
            ctxSignals.setLineDash([5, 3]);
            ctxSignals.strokeStyle = "white";
            window.OHLCData = OHLCData;
            SignalData.forEach(signal => {
                //console.log(SignalData);
               
                OHLCData.forEach(function(item,index){
                    // console.log(item)
                    if(item.TimeStamp==signal.TimeStamp){
                        var positionyval = getyposition(item);
                        var positionxval = getxpositionByIndex(OHLCData.length-index);
                        displaysignal(positionxval, positionyval, signal);
                        //ctxSignals.fillText("BY: " + EnterAt, cx(positionxval), cy(positionyval));
                    }
                    
                });
                
                if (signal.SignalLines != undefined) {
                    
                    signal.SignalLines.forEach(function (item,index) {
                        var xindex = OHLCData.findIndex(x => x.TimeStamp == item.TimeStamp)
                        
                        if (index == signal.SignalLines.length - 1)
                            xindex = xindex + 1;
                        
                        var posx = getxpositionByIndex(OHLCData.length - xindex);
                        var posy = getypositionByPrice(item.Price);
                        
                        if (index == 0) {
                            ctxSignals.beginPath();
                            ctxSignals.moveTo(cx(posx), cy(posy));
                        }
                        else {
                            ctxSignals.lineTo(cx(posx), cy(posy));
                            ctxSignals.stroke();
                        }
                        
                    });
                    
                }
                signal.SignalQuotes.forEach(function (item,index) {
                    
                    var xindex;
                    if (item.Index % 1 == "0.5") {
                        var SignalItem1 = signal.SignalLines[item.Index - 0.5];
                        var SignalItem2 = signal.SignalLines[item.Index + 0.5];
                        var xindex1 = OHLCData.findIndex(x => x.TimeStamp == SignalItem1.TimeStamp)
                        var xindex2 = OHLCData.findIndex(x => x.TimeStamp == SignalItem2.TimeStamp)
                        if (index+1 == signal.SignalLines.length - 1)
                            xindex2 = xindex2 + 1;
                        var xindex = (xindex1 + xindex2) / 2;
                        
                    }
                    else {
                        var SignalItem = signal.SignalLines[item.Index];
                        var xindex = OHLCData.findIndex(x => x.TimeStamp == SignalItem.TimeStamp)
                        if (index == signal.SignalLines.length - 1)
                            xindex = xindex + 1;
                    }
                    
                    
                    var posx = getxpositionByIndex(OHLCData.length - xindex);
                    var posy = getypositionByPrice(item.Price);
                    ctxSignals.fillStyle = "yellow";
                    ctxSignals.fillText(item.Text + ": "+item.Price, cx(posx + 1), cy(posy))
                });
                
                
                
            });
            //     //short
            //     ctxMain.beginPath();
            //     ctxMain.moveTo(cx(50),cy(50));
            //     ctxMain.lineTo(cx(54),cy(50));
            //     ctxMain.lineTo(cx(52),cy(54));
            //     ctxMain.lineTo(cx(50),cy(50));
            //     ctxMain.rect(cx(51.3), cy(50), cx(1.5), cy(-9));
            //     ctxMain.stroke();
            //     ctxMain.fillStyle="red";
            //     ctxMain.fill();
            // //long
            // ctxMain.beginPath();
            //     ctxMain.moveTo(cx(30),cy(30));
            //     ctxMain.lineTo(cx(34),cy(30));
            //     ctxMain.lineTo(cx(32),cy(26));
            //     ctxMain.lineTo(cx(30),cy(30));
            //     ctxMain.rect(cx(31.3), cy(30), cx(1.5), cy(9));
            //     ctxMain.stroke();
            //     ctxMain.fillStyle="green";
            //     ctxMain.fill();
        }
        function XLabelSelection(curdate){
            var labels={};
            
            
            if(["1","3","5","15","60"].indexOf(IntervalType)>-1)
            {
                labels.first = curdate.getHours()+"-"+curdate.getMinutes();
                labels.second = curdate.getDate();
            }
            else if(["day","week"].indexOf(IntervalType)>-1 )
            {
                labels.first = curdate.getDate();
                labels.second = monthNames[curdate.getMonth()]
            }
            else if(["month"].indexOf(IntervalType)>-1 )
            {
                labels.first = monthNames[curdate.getMonth()];
                labels.second = curdate.getFullYear();
            }
            else{
                labels.first = curdate.getDate()+"-"+curdate.getMonth()+"-"+curdate.getFullYear();
                labels.second = ""
            }
            
            return labels;
            // val = monthNames[datevalue.getMonth()]
            // return val;
        }
        var lastadded=30;
    this.DrawChart = function (inputdata, canvasMainInput, canvasLinesInput, canvasSignalsInput){
            canvasMain=canvasMainInput;
            canvasLines = canvasLinesInput;
            canvasSignals = canvasSignalsInput;
            ctxMain = canvasMain.getContext('2d');
            ctxLines = canvasLines.getContext('2d');
            ctxSignals = canvasSignals.getContext('2d');
            
            ctxMain.clearRect(0, 0, canvasMain.width, canvasMain.height);
            InitInputData(inputdata)
            recieveData();
            calculateMinMaxPrices();
            SetYValues();
            
                drawCanvas();
                drawPriceLines();
                drawCandles();
                drawHeader();
                showSignalData();
                $("#canvasSignals").mousemove(function(e){handleMouseMove(e);});
                
        }
        function InitInputData(alldata){
            OHLCData = alldata.OHLCData;
            SymbolName = alldata.SymbolName;
            SignalData = alldata.SignalData;
            IntervalType = alldata.IntervalType;
        }
        function SetYValues(){
            var diff = maxprice-minprice;
            var eachdiff = diff/totalYLinesLimit;
            var curprice=minprice;
            PricesForY.push(curprice.toFixed(2));
            for(var i=0;i<totalYLinesLimit;i++){
                curprice = curprice+eachdiff;
                PricesForY.push(curprice.toFixed(2));
            }
            // console.log(PricesForY);
        }
        function calculateMinMaxPrices(){
            maxprice=0;
            minprice=0;
            OHLCData.forEach(element => {
                if(element.High>maxprice || maxprice==0)
                maxprice=element.High;
                if(element.Low<minprice || minprice==0)
                minprice=element.Low;
                curdate = new Date(element.TimeStamp);
                if(curdate>maxdate || maxdate==undefined)
                maxdate=curdate;
                if(curdate<mindate || mindate==undefined)
                mindate=curdate;
            });
            var totaldiff = maxprice-minprice;
            var extramargin = totaldiff*0.20;
            maxprice=maxprice+extramargin;
            minprice=minprice-extramargin;
            totalpointslimit= OHLCData.length;
            var tempval = 80/OHLCData.length;
            candlewidth = tempval*0.80;
            canldeWidthWithMargin =tempval;
        }
        function recieveData(){
            const urlParams = new URLSearchParams(window.location.search);
            var sName = urlParams.get('SymbolName');
            if(sName==undefined)
            return;
            SymbolName=sName;
            var mydata = urlParams.get('data');
            if(mydata==undefined)
            return;
            
            if(mydata[0]=='"')
            mydata=mydata.substr(1,mydata.length-2);
            var dataobj = JSON.parse(decodeURIComponent(mydata));
            OHLCData = dataobj;
            
        }
        
        var actualindex;
        function handleMouseMove(evt){
            //console.log(Date.now())
            var canvas = canvasLines;
            var rect = canvasMain.getBoundingClientRect();
            
            var x= evt.clientX - rect.left;
            var y= evt.clientY - rect.top;
            // var x= evt.clientX;  
            // var y= evt.clientY;
            
            var PointerPrice = getyPriceByPosition(y);
            var XinPercent = rcx(x)
            // -0.01;
            var xindex = getxIndexByposition(XinPercent)
            // xindexchanged =xindex- (xindex*0.50);
            // var TimeStamp = OHLCData[xindex].TimeStamp;
            var limitedindex = xindex-Math.floor(xindex)
            if(limitedindex>0.60 || limitedindex<0.30)
            {
                actualindex = totalpointslimit- Math.round(xindex);
                //console.log("i is ",actualindex,"price is", PointerPrice);
            }
            
            var linesdraw=1;
            if(y>cy(90) || y<cy(10))
            return;
            if(x>cx(90) || x<cx(10))
            return;
            if(actualindex<0 || actualindex>=totalpointslimit)
            return;
            ctxLines.clearRect(0,0,canvasLines.width,canvasLines.height);
            
            
            ctxLines.beginPath();
            //horizontal
            ctxLines.moveTo(cx(10),y);
            ctxLines.lineTo(cx(90),y);
            //vertical
            ctxLines.moveTo(x,cy(10));
            ctxLines.lineTo(x,cy(90));
            ctxLines.setLineDash([5, 3]);
            // ctxLines.lineTo(cx(10),cy(90));
            // ctxLines.lineTo(cx(10),cy(10));
            ctxLines.strokeStyle="white";
            ctxLines.stroke();
            
            var OHLCDataItem = OHLCData[actualindex];
            var curdate = new Date(OHLCDataItem.TimeStamp);
            labels = XLabelSelection(curdate);
            ctxLines.beginPath();
            // ctxMain.moveTo(cx(50),cy(5))
            ctxLines.font= "10px Arial";
            ctxLines.fillStyle="white"
            ctxLines.textAlign = "center";
            ctxLines.fillText("DT: "+labels.first+" "+labels.second, cx(5), cy(10));
            ctxLines.fillText("O: "+OHLCDataItem.Open, cx(5), cy(14));
            ctxLines.fillText("H: "+OHLCDataItem.High, cx(5), cy(18));
            ctxLines.fillText("L: "+OHLCDataItem.Low, cx(5), cy(22));
            ctxLines.fillText("C: "+OHLCDataItem.Close, cx(5), cy(26));
            ctxLines.fillText("Pointer: "+PointerPrice.toFixed(2), cx(5), cy(36));
            // ctxMain.fillText(labels.second , cx(item.x), cy(98));
            ctxLines.stroke();
            
            // ctxMain.beginPath();
            // ctxMain.rect(x , y, cx(2), cy(2));
            // ctxMain.stroke();
        }
    }
var CandleChart = new CandleChart();
if(typeof(module)!="undefined")
module.exports = CandleChart;