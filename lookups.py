from upstox_api.api import *

intervalTypeMS = {
  "1min": 60000,
  "5min": 300000,
  "15min": 900000,
  "30min": 1800000,
  "1hour": 3600000,
}

intervalTypes = {
  "1min": OHLCInterval.Minute_1,
  "3min": OHLCInterval.Minute_3,
  "5min": OHLCInterval.Minute_5,
  "10min": OHLCInterval.Minute_10,
  "15min": OHLCInterval.Minute_15,
  "30min": OHLCInterval.Minute_30,
  "1hour": OHLCInterval.Minute_60,
  "day": OHLCInterval.Day_1,
  "week": OHLCInterval.Week_1,
  "month": OHLCInterval.Month_1,

}

